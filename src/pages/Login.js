import axios from "axios";
import React, { useState } from "react";
import { useHistory } from "react-router-dom";

export default function Login() {
  const history = useHistory();
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const login = async (e) => {
    e.preventDefault(e);

    await axios
      .post("http://localhost:3030/user/login", {
        email: email,
      })
      .then(() => {
        alert("SUKSES");
        history.push("/home");
      });
  };

  return (
    <div className="h-screen">
      <div class="w-full max-w-xs lg:max-w-sm mx-auto pt-[10%] pb-28">
        <form
          onSubmit={login}
          class="bg-white shadow-lg rounded px-8 pt-6 pb-8 mb-4"
        >
          <h1 className="text-center text-3xl font-semibold ">LOGIN</h1>
          <div class="mb-4 mt-9">
            <label
              class="block text-gray-700 text-sm font-bold mb-2"
              for="email"
            >
              Email
            </label>
            <input
              class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-teal-500 focus:shadow-outline"
              id="email"
              type="email"
              value={email}
              onChange={(e) => setEmail(e.target.value)}
              placeholder="Enter your email ..."
              required
            />
          </div>

          <div class="mb-6">
            <label
              class="block text-gray-700 text-sm font-bold mb-2"
              for="password"
            >
              Password
            </label>
            <input
              class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 mb-3 leading-tight focus:outline-teal-500 focus:shadow-outline"
              id="password"
              type="password"
              value={password}
              required
              onChange={(e) => setPassword(e.target.value)}
              placeholder="******************"
            />
          </div>
          <div class="flex items-center justify-between">
            <button
              class="bg-teal-500 hover:bg-teal-600 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
              type="submit"
            >
              Sign In
            </button>
            <a
              class="inline-block align-baseline font-bold text-sm text-teal-500 hover:text-teal-600"
              href="/register"
            >
              Daftar
            </a>
          </div>
        </form>
        <p class="text-center text-gray-500 text-xs">
          &copy;2023 Din.Ovita Seluruh hak cipta.
        </p>
      </div>
    </div>
  );
}
