import axios from "axios";
import React, { useState } from "react";
import { useHistory } from "react-router-dom";

export default function Register() {
  const [username, setUsername] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const history = useHistory();

  const register = async (e) => {
    e.preventDefault();

    const dataUser = {
      username: username,
      email: email,
      password: password,
    };

    const result = await axios
      .post("http://localhost:3030/user", dataUser)
      .then(() => {
        alert("SUKSES!");
        history.push("/login");
      })
      .catch((error) => {
        console.log(error);
      });
  };

  return (
    <div className="h-screen">
      <div class="w-full max-w-xs lg:max-w-sm mx-auto pt-16 pb-20">
        <form
          onSubmit={register}
          class="bg-white shadow-lg rounded px-8 pt-6 pb-8 mb-4"
        >
          <h1 className="text-center text-3xl font-semibold ">REGISTER</h1>
          <div class="mb-4 mt-9">
            <label
              class="block text-gray-700 text-sm font-bold mb-2"
              for="username"
            >
              Username
            </label>
            <input
              class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-teal-500 focus:shadow-outline"
              id="username"
              type="text"
              value={username}
              onChange={(e) => setUsername(e.target.value)}
              placeholder="Enter your username ..."
              required
            />
          </div>
          <div class="mb-4 mt-9">
            <label
              class="block text-gray-700 text-sm font-bold mb-2"
              for="email"
            >
              Email
            </label>
            <input
              class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-teal-500 focus:shadow-outline"
              id="email"
              type="email"
              value={email}
              onChange={(e) => setEmail(e.target.value)}
              placeholder="Enter your email ..."
              required
            />
          </div>

          <div class="mb-6">
            <label
              class="block text-gray-700 text-sm font-bold mb-2"
              for="password"
            >
              Password
            </label>
            <input
              class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 mb-3 leading-tight focus:outline-teal-500 focus:shadow-outline"
              id="password"
              type="password"
              value={password}
              required
              onChange={(e) => setPassword(e.target.value)}
              placeholder="******************"
            />
          </div>
          <div class="flex items-center justify-between">
            <button
              class="bg-teal-500 hover:bg-teal-600 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
              type="submit"
            >
              Sign Up
            </button>
            <a
              class="inline-block align-baseline font-bold text-sm text-teal-500 hover:text-teal-600"
              href="/login"
            >
              Masuk
            </a>
          </div>
        </form>
        <p class="text-center text-gray-500 text-xs">
          &copy;2023 Din.Ovita Seluruh hak cipta.
        </p>
      </div>
    </div>
  );
}
